from core.backup_helper import BackupService
from core.config import CONSUL_EXPORT_CONFIG
from core.consul_service import ConsulService
from core.secure_service import SecureConsulService
from core.yaml_helper import read_yaml_config

export_config_dict = read_yaml_config(CONSUL_EXPORT_CONFIG)

project_prefix = export_config_dict['export']['project_prefix']
part_name = export_config_dict['export']['part_name']
fields_to_secure = export_config_dict['secure']['fields']


cs = ConsulService()
items_by_pn = cs.get_all_items_by_prefix_contains_name(project_prefix, part_name)

bs = BackupService()
bs.backup_consul_kvlist_to_yamls(items_by_pn)

ss = SecureConsulService()
ss.delete_yaml_fields_in_kv_list(fields_to_secure, items_by_pn)

