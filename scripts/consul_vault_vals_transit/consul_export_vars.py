from core.backup_helper import BackupService
from core.config import CONSUL_EXPORT_CONFIG
from core.consul_service import ConsulService
from core.yaml_helper import read_yaml_config

""" выгрзуить поля из данных по всем сервисам согласно конфигу transit_config.yaml """

export_config_dict = read_yaml_config(CONSUL_EXPORT_CONFIG)

project_prefix = export_config_dict['export']['project_prefix']
part_name = export_config_dict['export']['part_name']
search_fields = export_config_dict['export']['fields']


cs = ConsulService()
items_by_pn = cs.get_all_items_by_prefix_contains_name(project_prefix, part_name)
bs = BackupService()
bs.backup_consul_kvlist_to_yamls(items_by_pn)
cs.export_fields(search_fields)
