import logging
from functools import reduce

import yaml

logger = logging.getLogger(__name__)


def deep_get(dictionary, *keys):
    """функция для поиска значения во вложенном словаре (например в структуре yaml)
    :arg keys пример - ['spring','username']
    """
    return reduce(lambda d, key: d.get(key) if d else None, keys, dictionary)

def extract_field_from_yaml(field_name, yaml_string):
    """:param field_name str -> Имя поля в формате .properties, пример = 'spring.username'"""
    temp_field = yaml.safe_load(yaml_string)
    for name_part in field_name.split('.'):
        temp_field = temp_field[name_part]
    logger.debug('из поля ' + field_name + ' извлечено значение ' + temp_field)
    return temp_field


def read_yaml_config(config_name):
    """:param config_name str -> Имя файла в директории откуда вызывается скрипт
    (правильно вызывать из корневой директории проекта)"""
    f = open(config_name, 'r')
    config__yaml_dict = yaml.safe_load(f)
    return config__yaml_dict
