import os
from datetime import datetime
import logging


import yaml

BACKUP_DIR = 'backups'

logger = logging.getLogger(__name__)

class BackupService:
    __iteration_dir_name = None

    def __init__(self):
        if not os.path.isdir(BACKUP_DIR):
            os.makedirs("backups")
        self.__create_bcp_dir_of_iteration()

    def __create_bcp_dir_of_iteration(self):
        """ создать директорию в папке бэкап """
        dir_name_pattern = BACKUP_DIR + '/' + datetime.now().strftime("%b-%d-%Y_%H-%M-%S")
        os.makedirs(dir_name_pattern)
        self.__iteration_dir_name = dir_name_pattern
        logger.debug('создана директория для бэкапа ' + dir_name_pattern)
        return dir_name_pattern

    def backup_consul_kvlist_to_yamls(self, kv_consul_list):
        for kv in kv_consul_list:
            new_filename = kv['Key'].replace('/', '-') + '.yaml'
            with open(self.__iteration_dir_name + '/' + new_filename, 'w') as file:
                yaml.dump(yaml.safe_load(kv['Value']), file)
                logger.debug('значения из консул сохранены')
