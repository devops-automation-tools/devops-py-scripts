import os

import consul
from jproperties import Properties

import logging

from core.config import CONSUL_EXPORT_FILE_NAME, CONSUL_EXPORT_DIR_NAME
from core.yaml_helper import extract_field_from_yaml


logger = logging.getLogger(__name__)


class ConsulService:
    consul_client = consul.Consul('localhost', 55199)

    __fetched_items = None

    def get_all_items_by_prefix(self, prefix):
        # вернуть массив значений без айди операции
        self.__fetched_items = self.consul_client.kv.get(prefix, 0, True)[1]
        logger.info('получены значения из consul с префиксом ' + prefix)
        return self.__fetched_items

    def get_all_items_by_prefix_contains_name(self, prefix, name):
        kv_list_items = self.get_all_items_by_prefix(prefix)
        logger.info('в названии которых есть значение ' + name)

        # инструкция по использованию фильтров https://www.pythontutorial.net/python-basics/python-filter-list/
        # лямбд https://www.geeksforgeeks.org/python-lambda-anonymous-functions-filter-map-reduce/
        filtered_list = list(filter(lambda kv_item: name in kv_item['Key'], kv_list_items))
        self.__fetched_items = filtered_list
        return filtered_list

    def export_fields(self, fieldnames_list):
        p = Properties()
        logger.info('извлекаем поля с именами - ' + str(fieldnames_list) + ' из значений полученных значений')
        for fieldname in fieldnames_list:
            # yaml_export_dict = {}
            for kv_item in self.__fetched_items:
                # взять название сервиса из ключа (стандартно второй с конца)
                item_service_name = None
                try:
                    item_service_name = kv_item['Key'].split('/')[-2]
                    extracted_field_value = extract_field_from_yaml(fieldname, kv_item['Value'])
                    p[item_service_name + '.' + fieldname] = extracted_field_value
                except:
                    logger.warning('в конфиге ' + item_service_name + ' не найдено значение ' + fieldname)
        if not os.path.isdir(CONSUL_EXPORT_DIR_NAME):
            os.makedirs(CONSUL_EXPORT_DIR_NAME)
        with open(CONSUL_EXPORT_DIR_NAME + '/' + CONSUL_EXPORT_FILE_NAME, "wb") as file:
            p.store(file, "utf-8")
